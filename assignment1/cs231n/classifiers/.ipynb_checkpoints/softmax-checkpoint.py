from builtins import range
import numpy as np
from random import shuffle
from past.builtins import xrange

def softmax_loss_naive(W, X, y, reg):
    """
    Softmax loss function, naive implementation (with loops)

    Inputs have dimension D, there are C classes, and we operate on minibatches
    of N examples.

    Inputs:
    - W: A numpy array of shape (D, C) containing weights.
    - X: A numpy array of shape (N, D) containing a minibatch of data.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c means
      that X[i] has label c, where 0 <= c < C.
    - reg: (float) regularization strength

    Returns a tuple of:
    - loss as single float
    - gradient with respect to weights W; an array of same shape as W
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)
    
    scores = X.dot(W) # NxD . DxC = NxC

    #############################################################################
    # TODO: Compute the softmax loss and its gradient using explicit loops.     #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    num_train = X.shape[0]
    num_class = W.shape[1]
    
    for i in range(num_train):
        f = scores[i] - np.max(scores[i]) # avoid numerical instability: http://cs231n.github.io/linear-classify/
        loss += -f[y[i]] + np.log(np.sum(np.exp(f)))
        for j in range(num_class):
            dW[:, j] += X[i]*np.exp(f)[j] / np.sum(np.exp(f))
        dW[:, y[i]] += -X[i]
        
    loss /= num_train
    loss += np.sum(W*W) * reg
    
    dW /= num_train
    dW += 2 * reg * W
    
    # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    return loss, dW


def softmax_loss_vectorized(W, X, y, reg):
    """
    Softmax loss function, vectorized version.

    Inputs and outputs are the same as softmax_loss_naive.
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)
    
    num_train = X.shape[0]
    num_class = W.shape[1]
    
    scores = X.dot(W) # NxD . DxC = NxC

    #############################################################################
    # TODO: Compute the softmax loss and its gradient using no explicit loops.  #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

#     f = scores - np.max(scores, axis = 0, keepdims=True) # NxC
#     softmax = np.exp(f) / np.sum(np.exp(f), axis=1, keepdims=True) # NxC
    
#     loss = np.sum(-f[np.arange(y.shape[0]), y] + np.log(np.sum(np.exp(f), axis=1)))
    
#     softmax_alias = np.zeros_like(softmax) # NxC
#     softmax_alias[np.arange(X.shape[0]), y] = -1
#     dW = X.T.dot(softmax) # DxC
#     dW += X.T.dot(softmax_alias) # DxN . NxC = DxC
    
#     loss /= num_train
#     loss += reg*np.sum(W*W)
    
#     dW /= num_train
#     dW += 2*reg*W
    scores = scores - np.max(scores, axis=1, keepdims=True) # NxC
    
    sum_exp_scores = np.exp(scores).sum(axis=1, keepdims=True) # Nx1
    softmax_matrix = np.exp(scores)/sum_exp_scores # NxC
    loss = np.sum(-np.log(softmax_matrix[np.arange(num_train), y])) # scalar

    # Weight Gradient
    softmax_matrix[np.arange(num_train),y] -= 1 # NxC
    dW = X.T.dot(softmax_matrix) # DxN . NxC = DxC

    # Average
    loss /= num_train
    dW /= num_train

    # Regularization
    loss += reg * np.sum(W * W)
    dW += reg * 2 * W 
    
    # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    return loss, dW
